/******************************************************************************
 * Copyright (C) 2010-2018 CERN. All rights not expressly granted are reserved.
 * <p/>
 * This file is part of the CERN Control and Monitoring Platform 'C2MON'.
 * C2MON is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the license.
 * <p/>
 * C2MON is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * <p/>
 * You should have received a copy of the GNU Lesser General Public License
 * along with C2MON. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
package cern.c2mon.configuration.examples.rest;

import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import cern.c2mon.client.core.C2monServiceGateway;
import cern.c2mon.client.core.service.ConfigurationService;
import cern.c2mon.shared.client.configuration.api.tag.DataTag;
import cern.c2mon.shared.common.datatag.DataTagAddress;

/**
 * A simple example how to statically inject a configuration for the REST DAQ to C2MON.
 * <p>
 * To run this example you have first to start locally a C2MON server
 *
 * @author Matthias Braeger
 */
@Slf4j
public class ConfigureRestDaq {

  /** The DAQ process name that you have to use starting the REST DAQ */
  private static String processName = "P_REST";

  /** The Equipment name is required to initialise the DAQ process with a REST Equipment. It can also be changed. */
  private static String equipmentName = "E_REST";

  /** A default address for all tags */
  private static HashMap<String, String> address = getDataTagAddress();


  /**
   * Run this main to configure the REST DAQ process on C2MON
   */
  public static void main(String[] args) {

    ConfigurationService configurationService = C2monServiceGateway.getConfigurationService();

    log.info("Remove Process {}", processName);
    // For simplicity we start from scratch every time
    configurationService.removeProcess(processName);

    log.info("Create Process {}", processName);
    configurationService.createProcess(processName);

    log.info("Create Equipment {}", equipmentName);
    configurationService.createEquipment(processName, equipmentName, "cern.c2mon.daq.rest.RestMessageHandler");

    configurationService.createDataTag(equipmentName, createDataTagConfiguration("tag1", Boolean.class, "Optional tag description"));

    log.info("Done! You can now start the REST DAQ called {}", processName);
    System.exit(0);
  }

  /**
   * Creates a default address for the DataTag configuration that allows sending
   * HTTP POST message to the REST DAQ
   *
   * Please read also the README.md
   *
   * @return A {@link HashMap} with the address specification
   */
  private static HashMap<String, String> getDataTagAddress() {
    HashMap<String, String> address = new HashMap<>();
    address.put("mode", "POST");
    // OPTIONAL: when putting this we expect every 30 second a message, otherwise the Tag gets invalidated
    address.put("postFrequency", "30");
    return address;
  }


  /**
   * Creates a new DataTag configuration.
   * This method can easily be called in a loop to create many tags
   *
   * @param tagName tag name
   * @param valueType class type of tag value
   * @param description static tag description
   * @return The {@link DataTag} description
   */
  private static DataTag createDataTagConfiguration(String tagName, Class<?> valueType, String description) {
    log.info("Create DataTag {}", tagName);
    return DataTag.create(tagName, valueType, new DataTagAddress(address))
                         .description(description)
                         // you can also add metadata later with a tag update
                         .addMetadata("Responsible", "Plastic Omnium")
                         .build();
  }
}
