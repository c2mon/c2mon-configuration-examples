# C2MON configuration examples
[![build status](https://gitlab.cern.ch/c2mon/c2mon-configuration-examples/badges/master/build.svg)](https://gitlab.cern.ch/c2mon/c2mon-configuration-examples/commits/master)

This idea is to put here code snippets to demonstrate how to make use the C2MON Configuration API.

Currently, this project has only an example for configuring the REST DAQ

## C2MON REST DAQ configuration example

A simple example of how to statically inject a configuration for the REST DAQ to C2MON.

After running the provided code you can start a REST DAQ with name `P_REST` and send HTTP POST messages to it.
The example is only configuring one boolean tag with name `tag1`.
However, the code can easily be modified to add more DataTags with different names and value types.
 
### Prerequirements

To run this example you have first to start locally a C2MON server.
Please read therefore:
http://cern.ch/c2mon/c2mon/docs/getting-started/

In addition we need at least Java 8 and Maven installed

### C2MON Maven settings

As C2MON is not (yet) storing the Artifacts in Central Maven Repository, please use the [Maven settings](settings.xml) file of this project to compile the code.



### Finding the code

The code of this example is located in the following package:

[`cern.c2mon.configuration.examples.rest`](https://gitlab.cern.ch/c2mon/c2mon-configuration-examples/tree/master/src/main/java/cern/c2mon/configuration/examples/rest)

### Running the configuration example

```bash
git clone ssh://git@gitlab.cern.ch:7999/c2mon/c2mon-configuration-examples.git
cd c2mon-configuration-examples/
mvn clean package --settings settings.xml
java -jar target/c2mon-configuration-examples-rest-<version>.jar
```

Please note that in order to keep the configuration also after C2MON restart, a
proper C2MON database has to be configured or HSQL persistence must be enabled.
For more information please read:
http://cern.ch/c2mon/docs/user-guide/server/database/

#### Starting the REST DAQ

Now we can start the REST DAQ process that we just configured. The simplest is to make use the docker image. The only thing we have to provide is the process name we configured, which is `P_REST`.
Again, we assume that the C2MON server is running locally.

```
docker run --rm --name daq-rest -it --net=host -e "C2MON_DAQ_JMS_URL=failover:tcp://localhost:61616" gitlab-registry.cern.ch/c2mon/c2mon-daq-rest bin/C2MON-DAQ-STARTUP.jvm -f P_REST
```

### What's next?

Now, all is ready to send data e.g. from a script via a HTTP POST to C2MON.
How to do that is explained in the [README](https://gitlab.cern.ch/c2mon/c2mon-daq-rest/blob/master/README.md) file of [c2mon-daq-rest](https://gitlab.cern.ch/c2mon/c2mon-daq-rest).